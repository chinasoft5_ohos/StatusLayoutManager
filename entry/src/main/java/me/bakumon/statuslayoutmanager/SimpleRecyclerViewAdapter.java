package me.bakumon.statuslayoutmanager;

import me.bakumon.statuslayoutmanager.circleimageview.CircleImageView;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * lipeiquan
 *
 * @since 2021-07-16
 */
public class SimpleRecyclerViewAdapter extends BaseItemProvider {

    private Context mContext;
    private List<SimpleData.Song> mList;
    private int mLayoutId;

    public SimpleRecyclerViewAdapter(Context context, List<SimpleData.Song> list, int layoutId) {
        this.mContext = context;
        this.mList = list;
        this.mLayoutId = layoutId;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(mContext).parse(mLayoutId,
                    null, false);
        }

        CircleImageView avatar = (CircleImageView) component.findComponentById(ResourceTable.Id_avatar);
        avatar.setPixelMap(mList.get(i).drawableResID);
        Text test = (Text) component.findComponentById(ResourceTable.Id_text1);
        test.setText(mList.get(i).songName);
        return component;
    }
}
