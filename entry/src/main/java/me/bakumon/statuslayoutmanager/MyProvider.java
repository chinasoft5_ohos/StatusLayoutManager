/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.bakumon.statuslayoutmanager;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * @since 2021-07-15
 */
public class MyProvider extends BaseItemProvider {

    private List<String> list;
    private WeakReference<Context> context;

    public MyProvider(Context context, List<String> list) {
        this.context = new WeakReference(context);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        if (component == null) {
            component = LayoutScatter.getInstance(context.get()).parse(ResourceTable.Layout_layout_spinner_item,
                    null, false);
        }

        Text mItemName = (Text) component.findComponentById(ResourceTable.Id_tv_item_name);
        mItemName.setText(list.get(i));
        Component finalComponent = component;
        finalComponent.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component c, TouchEvent touchEvent) {
                int action = touchEvent.getAction();
                ShapeElement shapeElement = null;
                if (action == TouchEvent.PRIMARY_POINT_DOWN) {
                    shapeElement = new ShapeElement();
                    shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#EBEBEB")));
                } else if (action == TouchEvent.PRIMARY_POINT_UP || action == TouchEvent.OTHER_POINT_UP) {
                    shapeElement = new ShapeElement();
                    shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
                }
                if (shapeElement != null) {
                    if (i == list.size() - 1 || i == 0) {
                        shapeElement.setCornerRadius(10);
                    }
                    finalComponent.setBackground(shapeElement);
                }
                return true;
            }
        });
        return component;
    }
}
