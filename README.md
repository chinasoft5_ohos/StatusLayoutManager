# StatusLayoutManager

#### 项目介绍
- 项目名称：StatusLayoutManager
- 所属系列：openharmony的第三方组件适配移植
- 功能：切换不同的数据状态布局，包含加载中、空数据和出错状态。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.0.4

#### 效果演示
![StatusLayoutManager](/screenshots/StatusLayoutManager.gif)


#### 安装教程
1.在项目根目录下的build.gradle文件中，
 ```

allprojects {
   repositories {
       maven {
          url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
       }
   }
}

 ```
 2.在entry模块的build.gradle文件中，
  ```
dependencies {
   implementation 'com.gitee.chinasoft_ohos:StatusLayoutManager:0.0.1-SNAPSHOT'   
   ......  
}
   ```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 使用说明

初始化
```
       new StatusLayoutManager.Builder(mRecyclerView)
                       .setOnStatusChildClickListener(new OnStatusChildClickListener() {
                           @Override
                           public void onEmptyChildClick(Component view) {
                               showToastDialog(getContext().getString(ResourceTable.String_reload_empty));
                               statusLayoutManager.showLoadingLayout();
                               getData(1000);
                           }
       
                           @Override
                           public void onErrorChildClick(Component view) {
                               showToastDialog(getContext().getString(ResourceTable.String_reload_error));
                               statusLayoutManager.showLoadingLayout();
                               getData(1000);
                           }
       
                           @Override
                           public void onCustomerChildClick(Component view) {
                               if (view.getId() == ResourceTable.Id_tv_customer) {
                                   showToastDialog(getContext().getString(ResourceTable.String_request_access));
                               } else if (view.getId() == ResourceTable.Id_tv_customer1) {
                                   showToastDialog(getContext().getString(ResourceTable.String_switch_account));
                               }
       
                           }
                       })
                       .build();
```
    
#### 测试信息

CodeCheck代码测试无异常  

CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

#### 版本迭代
- 0.0.1-SNAPSHOT  

#### 版权和许可信息  

   MIT License
   
   Copyright (c) 2017 食梦兽
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in all
   copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   SOFTWARE.
 


